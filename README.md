# Alfa Web3 SDK

A simple to use Blockchain SDK to manage assets on EVM-compatible Blockchains and on IPFS

-	Connect to any EVM Blockchain
-	Utilities (Streamlining Asset Management)
o	Create, manage, burn assets on Blockchain and IPFS
o	
-	Intelligent Asset Cache (Metadata on EVM, Files on IPFS)
-	Asset Repository
-	Full IPFS Node (File management, incl. MFS)
-	Secure Vault
-	Orderbook for decentralized Exchange
-	Documentation: [https://docs.web3.alfabc.io](https://docs.web3.alfabc.io)
